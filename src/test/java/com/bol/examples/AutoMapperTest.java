package com.bol.examples;

/*
 * (C) Copyright 2016 Edwin Janssen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class AutoMapperTest {

    @Test
    public void shouldReturnConvertedList() throws Exception {

        Map<Predicate<String>, Supplier<Integer>> mapper = new HashMap<Predicate<String>, Supplier<Integer>>() {
            {
                put(s -> s.equals("a"), () -> 1);
                put(s -> s.equals("b"), () -> 2);
                put(s -> s.equals("c"), () -> 3);
                put(s -> s.equals("d"), () -> 4);
            }
        };

        AutoMapper.Builder<String, Integer> builder = new AutoMapper.Builder<>();
        AutoMapper<String, Integer> autoMapper = builder
                .mapper(mapper)
                .fallback(999)
                .build();

        Assert.assertEquals(
                Arrays.asList(1, 2, 3, 4, 999, 999),
                autoMapper.convert(
                        Arrays.asList("a", "b", "c", "d", "e", "f")
                )
        );
    }

    @Test
    public void shouldReturnConvertListOfWords() throws Exception {

        Map<Predicate<Word>, Supplier<Integer>> mapper = new HashMap<Predicate<Word>, Supplier<Integer>>() {
            {
                put(w -> w.getValue().equals("edwin"), () -> 1);
            }
        };

        AutoMapper.Builder<Word, Integer> builder = new AutoMapper.Builder<>();
        AutoMapper autoMapper = builder
                .mapper(mapper)
                .fallback(999)
                .build();

        Assert.assertEquals(
                Arrays.asList(1, 999),
                autoMapper.convert(
                        Arrays.asList(new Word("edwin"), new Word("linda"))
                )
        );
    }
}