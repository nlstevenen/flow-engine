package com.bol.examples;

/*
 * (C) Copyright 2016 Edwin Janssen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

@SuppressWarnings("unchecked")
public class AutoMapper<P, S> {

    private final Map<Predicate<P>, Supplier<S>> mapper;
    private final Map.Entry<Predicate<P>, Supplier<S>> other;

    private AutoMapper(Builder builder) {
        checkNotNull(builder.mapper);
        checkNotNull(builder.other);

        this.mapper = builder.mapper;
        this.other = builder.other;
    }

    public List<S> convert(P[] items) {
        return stream(Arrays.asList(items));
    }

    public List<S> convert(List<P> values) {
        return stream(values);
    }

    private List<S> stream(List<P> values) {
        return values.parallelStream()
                .map(s -> mapper.entrySet()
                        .parallelStream()
                        .filter(entry -> entry.getKey()
                                .test(s))
                        .findFirst()
                        .orElse(other)
                        .getValue()
                        .get())
                .collect(Collectors
                        .toList());
    }

    public static class Builder<P, S> {

        private Map<Predicate<P>, Supplier<S>> mapper;
        private Map.Entry<Predicate<P>, Supplier<S>> other;

        public Builder<P, S> mapper(Map<Predicate<P>, Supplier<S>> mapper) {
            this.mapper = mapper;
            return this;
        }

        public Builder<P, S> fallback(S fallback) {
            this.other = new Map.Entry<Predicate<P>, Supplier<S>>() {
                @Override
                public Predicate<P> getKey() {
                    return null;
                }

                @Override
                public Supplier<S> getValue() {
                    return () -> fallback;
                }

                @Override
                public Supplier<S> setValue(Supplier<S> value) {
                    return null;
                }
            };
            return this;
        }

        public AutoMapper<P, S> build() {
            return new AutoMapper(this);
        }
    }
}
